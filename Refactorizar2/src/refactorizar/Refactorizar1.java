package refactorizar;

import java.util.Scanner;

public class Refactorizar1 {
	static Scanner a;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*las variables tienen que tener un nombre que indique de que estamos
		hablando, la cantidad de alumno es una costante la antigua letra a es habria que cambiarlo como teclado,
		solo una variable por linea*/
		Scanner teclado = new Scanner(System.in);
		
		final int cantidad_maxima_alumnos =10;
			
		int[] alumnos= new int[10];
		peticionNotaMedia(teclado, cantidad_maxima_alumnos, alumnos);	
		
		System.out.println("El resultado es: " + muestraAlumnos(alumnos));
		
		teclado.close();
	}
	private static void peticionNotaMedia(Scanner teclado, final int cantidad_maxima_alumnos, int[] alumnos) {
		for(int n=0; n<cantidad_maxima_alumnos;n++)
		{
			System.out.println("Introduce nota media de alumno");
			alumnos[n] = teclado.nextInt();
		}
	}
	static double muestraAlumnos(int []alumnos)
	{
		double c = 0;
		for(int a = 0 ; a < 10 ; a++) 
		{
			c += alumnos[a];
		}
		return (c/10);
	}
}
